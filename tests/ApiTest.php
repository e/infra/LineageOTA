<?php

require_once 'vendor/autoload.php';
use flight\Engine; 

use flight\net\Request;
use flight\net\Router;
use flight\core\Dispatcher;
use PHPUnit\Framework\TestCase;
use \JX\CmOta\CmOta;
use \JX\CmOta\Helpers\Builds;
use Monolog\Logger;

class ApiTest extends TestCase {
 
    /**
     * @var Engine
     */
    private $app;
    private Router $router;
    private Request $request;
    private Dispatcher $dispatcher;

    protected function setUp(): void
    {
        
        $this->router = new Router();
        $this->request = new Request();
        $this->dispatcher = new Dispatcher();

        $this->romType = 'dev';
        $this->deviceType = 'Amber';
        $this->incrementalVersion = '0b971351f3';

        $this->customData = array(
            'params' => array(
              'device' => $this->deviceType,
              'channels' => array(
                $this->romType,
              ),
              'source_incremental' => $this->incrementalVersion,
            ),
        );

        $this->logger = new Logger('main');
        $this->getCmotaInstance();
    }

    public function getCmotaInstance(){
        Flight::register('cmota', '\JX\CmOta\CmOta',array($this->logger));
        return Flight::cmota();
    }
    public function getBuildInstance(){
        Flight::register('builds', '\JX\CmOta\Helpers\Builds',array($this->logger));
        return Flight::builds();
    }

    public function testCheckIfBuildValid() {
        $builds = $this->getBuildInstance();
        $builds->setPostData($this->customData);
        $records = $builds->get();
        $this->assertGreaterThan( 0, sizeof($records),'Builds are not valid!'); 
    }

     public function testCheckIfCorrectAttribute() {
        $attributes = array("timestamp" , "md5sum" , "url");  // You can change attributes here

        $builds = $this->getBuildInstance();
        $builds->setPostData($this->customData);
        $records = $builds->get();
        if(sizeof($records)){
            foreach ($attributes as $attribute) {
                $this->assertArrayHasKey($attribute, $records[0], "Builds doesn't contains '".$attribute."' as key");
            }
        }else{
            $this->assertFalse('Builds');
        }
    }
 

}