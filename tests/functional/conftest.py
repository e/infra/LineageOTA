import pytest


class Context:
    def __init__(self, config):
        self.domain = config.getoption("--ota-domain")
        assert self.domain is not None, "OTA server domain has to be defined"
        self.device = config.getoption("--ota-device")
        self.channel = config.getoption("--ota-channel")
        self.url = f"https://{self.domain}"


def pytest_addoption(parser):
    parser.addoption(
        "--ota-domain", action="store", default=None, help="OTA server domain"
    )
    parser.addoption(
        "--ota-device", action="store", default="FP3", help="OTA device name"
    )
    parser.addoption(
        "--ota-channel", action="store", default="stable", help="OTA channel name"
    )


@pytest.fixture
def ctx(request):
    context = Context(request.config)
    yield context
