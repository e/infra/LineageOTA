import httpx
import pytest


@pytest.fixture
def ctx(ctx):
    ctx.cli = httpx.Client(base_url=f"{ctx.url}/api/v1", timeout=60)
    yield ctx


def test_list_device_builds(ctx):
    res = ctx.cli.get(f"/{ctx.device}/{ctx.channel}")
    assert res.status_code == httpx.codes.OK
    data = res.json()
    assert not data["error"]
    assert len(data["response"])
    build = data["response"][-1]
    assert build["channel"] == ctx.channel
    assert build["romtype"] == ctx.channel
    assert build["url"].startswith(ctx.url)
    assert build["filename"].startswith("e-")


def test_list_build_with_incremental_update_field(ctx):
    # list and sort all builds by datetime
    res = ctx.cli.get(f"/{ctx.device}/{ctx.channel}")
    assert res.status_code == httpx.codes.OK
    data = sorted(res.json()["response"], key=lambda d: d["datetime"])
    assert len(data) >= 2

    # list build with incremental field
    build = data[-2]
    res = ctx.cli.get(f"/{ctx.device}/{ctx.channel}/{build['incremental']}")
    assert res.status_code == httpx.codes.OK
    data = res.json()
    assert len(data["response"]) == 1


def test_FP4_upgrade_from_given_build(ctx):
    """FP4 A12 upgrade is allowed only from 1.5-r build"""
    # create a map about /e/OS builds
    res = ctx.cli.get("/FP4/stable")
    assert res.status_code == httpx.codes.OK
    assert len(res.json()["response"])
    builds = {
        f"{d['version']}-{d['pre_version']}-{d['android_version']}": d
        for d in res.json()["response"]
    }

    # ensure that A12 builds are not listed with version <= 1.4-r
    build14r = builds["1.4--11"]
    res = ctx.cli.get(f"/FP4/stable/{build14r['incremental']}")
    assert res.status_code == httpx.codes.OK
    data = res.json()["response"]
    a12builds = [d for d in data if d["android_version"] == "12"]
    assert len(a12builds) == 0

    # ensure that A12 builds are listed with version == 1.5-r
    build15r = builds["1.5--11"]
    res = ctx.cli.get(
        f"/FP4/stable/{build15r['incremental']}",
        headers={"User-Agent": "eOS v1.5"},
    )
    assert res.status_code == httpx.codes.OK
    data = res.json()["response"]
    a12builds = [d for d in data if d["android_version"] == "12"]
    a11builds = [d for d in data if d["android_version"] == "11"]
    assert len(a11builds) == 0
    assert len(a12builds) >= 1
