<?php

require_once 'vendor/autoload.php';
use flight\Engine; 

use flight\net\Request;
use flight\net\Router;
use flight\core\Dispatcher;
use PHPUnit\Framework\TestCase;
use \JX\CmOta\CmOta;
use \JX\CmOta\Helpers\Builds;
use Monolog\Logger;

class BuildTest extends TestCase {
 
    /**
     * @var Engine
     */
    private $app;
    private Router $router;
    private Request $request;
    private Dispatcher $dispatcher;

    protected function setUp(): void
    {
        
        $this->router = new Router();
        $this->request = new Request();
        $this->dispatcher = new Dispatcher();

        $this->romType = 'dev';
        $this->deviceType = 'Amber';
        $this->incrementalVersion = '0b971351f3';

        $this->customData = array(
            'params' => array(
              'device' => $this->deviceType,
              'channels' => array(
                $this->romType,
              ),
              'source_incremental' => $this->incrementalVersion,
            ),
        );

        $this->logger = new Logger('main');
        $this->getCmotaInstance();
    }

    public function getCmotaInstance(){
        Flight::register('cmota', '\JX\CmOta\CmOta',array($this->logger));
        return Flight::cmota();
    }
    public function getBuildInstance(){
        Flight::register('builds', '\JX\CmOta\Helpers\Builds',array($this->logger));
        return Flight::builds();
    }
    
    // newer than (timestamp check)
    public function testCheckTimestamp() { 
        $builds = $this->getBuildInstance();
        $builds->setPostData($this->customData);
        $records = $builds->get();
        $currentTimestamp = strtotime(date('Y-m-d H:i:s'));
         
        foreach ($records as $record) {
           $this->assertGreaterThan($record['timestamp'], $currentTimestamp, 'Build is not new!'); 
        }
        
    }

   // check Android version if it is same or not
    public function testCheckAndroidVersion() { 
        $builds = $this->getBuildInstance();
        $builds->setPostData($this->customData);
        $records = $builds->get();
        $expectedVersion = '0.16';
        foreach ($records as $record) {
            $this->assertEquals($expectedVersion , $record['version'] ,$expectedVersion.' & '.$record['version'] .' Android Versions are different!');
        }
    }

}