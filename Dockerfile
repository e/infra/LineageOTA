FROM php:7.4-apache
MAINTAINER Julian Xhokaxhiu <info at julianxhokaxhiu dot com>

# internal variables
ENV HTML_DIR /var/www/html
ENV FULL_BUILDS_DIR $HTML_DIR/builds/full

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --fix-missing \
    apt-utils \
    gnupg

RUN apt-get update \
    && apt-get install -y git zlib1g-dev libzip-dev\
    && docker-php-ext-install zip\
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false zlib1g-dev libzip-dev

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# set the working directory
WORKDIR $HTML_DIR
# add all the project files
COPY . $HTML_DIR

# enable mod_rewrite
RUN a2enmod rewrite
COPY conf/perf.conf /etc/apache2/conf-available/perf.conf
RUN a2enconf perf

# install the PHP extensions we need
RUN apt-get update \
        && apt-get install nano libzip4\
        && buildDeps=" \
                zlib1g-dev libzip-dev \
        " \
        && apt-get install -y git $buildDeps --no-install-recommends \
        && rm -r /var/lib/apt/lists/* \
        \
        && docker-php-ext-install zip \
        \
        && docker-php-source delete \
        && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false $buildDeps 

# install latest version of composer
ADD https://getcomposer.org/composer.phar /usr/local/bin/composer
RUN chmod 0755 /usr/local/bin/composer

# add all the project files
COPY . $HTML_DIR

# enable indexing for Apache
RUN sed -i "1s;^;Options +Indexes\n\n;" .htaccess

# install dependencies
RUN composer install --no-plugins --no-scripts

# fix permissions
RUN chmod -R 0775 /var/www/html \
    && chown -R www-data:www-data /var/www/html

