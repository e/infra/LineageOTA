<?php
    /*
        The MIT License (MIT)

        Copyright (c) 2020 Julian Xhokaxhiu

        Permission is hereby granted, free of charge, to any person obtaining a copy of
        this software and associated documentation files (the "Software"), to deal in
        the Software without restriction, including without limitation the rights to
        use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
        the Software, and to permit persons to whom the Software is furnished to do so,
        subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
        FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
        COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
        IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
        CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */

    namespace JX\CmOta\Helpers;

    use \Flight;

    class Build
    {
        private $apiLevel = -1;
        private $channel = '';
        private $model = '';
        private $filename = '';
        private $url = '';
        private $changelogUrl = '';
        private $timestamp = '';
        private $incremental = '';
        private $filePath = '';
        private $buildProp = '';
        private $confProp = '';
        private $uid = null;
        private $size = '';
        private $logger = null;
        private $migrationFilePath = '';

        /**
         * Constructor of the Build class.
         * Here all the information about the current build will be collected
         * and make them available for the next time.
         * @param type $fileName The current filename of the build
         * @param type $physicalPath The current path where the build lives
         */
        public function __construct($fileName, $physicalPath, $logger, $shouldDisplayPatch = false)
        {
            $this->logger = $logger;

            /*
                $tokens Schema:

                array(
                    1 => [TYPE] (ex. cm, lineage, etc.)
                    2 => [VERSION] (ex. 10.1.x, 10.2, 11, etc.)
                    3 => [DATE OF BUILD] (ex. 20140130)
                    4 => [CHANNEL OF THE BUILD] (ex. RC, RC2, NIGHTLY, etc.)
                    5 =>
                      CM => [SNAPSHOT CODE] (ex. ZNH0EAO2O0, etc.)
                      LINEAGE => [MODEL] (ex. i9100, i9300, etc.)
                    6 =>
                      CM => [MODEL] (ex. i9100, i9300, etc.)
                      LINEAGE => [SIGNED] (ex. signed)
                )
            */
            preg_match_all('/(e)-([0-9\.]+)-?((alpha|beta|rc)(\.\d)?)?-\D-?([\d_]+)?-([\w+]+)-([A-Za-z0-9]+)?-?([\w+]+)?/', $fileName, $tokens);
            $this->filePath = (substr($physicalPath, -1) === '/') ? $physicalPath . $fileName : $physicalPath . '/' . $fileName;
            $tokens = $this->formatTokens($tokens);

            $this->migrationFilePath = $physicalPath . '/migration_paths.json';
            $this->channel = $this->_getChannel(str_replace(range(0, 9), '', $tokens[7]), $tokens[1], $tokens[2]);
            $this->filename = $fileName;

            // Try to load the build.prop from two possible paths:
            // - builds/CURRENT_ZIP_FILE.zip/system/build.prop
            // - builds/CURRENT_ZIP_FILE.zip.prop ( which must exist )
            $this->buildProp = explode("\n", @file_get_contents($this->getPropFilePath()));
            $this->confProp = $this->getConfProp();
            // Try to fetch build.prop values. In some cases, we can provide a fallback, in other a null value will be given
            $this->timestamp = intval($this->getBuildPropValue('ro.build.date.utc') ?? filemtime($this->filePath));
            $this->incremental = $this->getBuildPropValue('ro.build.version.incremental') ?? '';
            $this->apiLevel = $this->getBuildPropValue('ro.build.version.sdk') ?? '';
            $this->model = $this->getBuildPropValue('ro.lineage.device') ?? $this->getBuildPropValue('ro.cm.device') ?? ($tokens[1] == 'cm' ? $tokens[9] : $tokens[8]);
            $this->version = $tokens[2];
            if(substr_count($this->version, ".") > 1 && !$shouldDisplayPatch) { 
               $this->version = substr($this->version, 0, strrpos($this->version, '.'));//remove maintenance number when using an old updater to avoid crash
            }
            $this->preversion = $tokens[3];
            $this->displayVersion = $this->getBuildPropValue('ro.cm.display.version') ?? $this->getBuildPropValue('ro.lineage.display.version') ?? '';
            $this->androidVersion = $this->getBuildPropValue('ro.build.version.release') ?? '';
            $this->uid = hash('sha256', $this->timestamp.$this->model.$this->apiLevel, false);
            $this->size = filesize($this->filePath);
            $position = strrpos($physicalPath, '/builds/full');
            if ($position === false) {
                $this->url = $this->_getUrl('', Flight::cfg()->get('buildsPath'));
            } else {
                $this->url = $this->_getUrl('', Flight::cfg()->get('basePath') . substr($physicalPath, $position));
            }
            $this->changelogUrl = $this->_getChangelogUrl();
            if (!file_exists($this->filePath . '.prop')) {
                $prop = "";
                $prop.= 'ro.build.date.utc='.$this->timestamp."\n";
                $prop.='ro.build.version.incremental='.$this->incremental."\n";
                $prop.='ro.build.version.sdk='.$this->apiLevel."\n";
                if (!empty($this->getBuildPropValue('ro.lineage.device'))) {
                    $prop.='ro.lineage.device='.$this->model."\n";
                } elseif (!empty($this->getBuildPropValue('ro.cm.device'))) {
                    $prop.='ro.cm.device='.$this->model."\n";
                }
                if (!empty($this->getBuildPropValue('ro.cm.display.version'))) {
                    $prop.='ro.cm.display.version='.$this->displayVersion."\n";
                } elseif (!empty($this->getBuildPropValue('ro.lineage.display.version'))) {
                    $prop.='ro.lineage.display.version='.$this->displayVersion."\n";
                }
                $prop.='ro.build.version.release='.$this->androidVersion."\n";
                file_put_contents($this->filePath . '.prop', $prop);
            }
        }

        /**
         * Check if the current build is valid within the current request
         * @param type $params The params dictionary inside the current POST request
         * @return boolean True if valid, False if not.
         */
        public function isValid($params)
        {
            if ($params === null) {
                return true;
            }  // Assume valid if no parameters

            $ret = false;

            if ($params['device'] == $this->model || $params['device'] === "2e" && $this->model === "zirconia") {
                if (count($params['channels']) > 0) {
                    foreach ($params['channels'] as $channel) {
                        if (strtolower($channel) == $this->channel) {
                            $ret = true;
                        }
                    }
                }
            }

            return $ret;
        }

        /**
         * Create a delta build based from the current build to the target build.
         * @param type $targetToken The target build from where to build the Delta
         * @return array/boolean Return an array performatted with the correct data inside, otherwise false if not possible to be created
         */
        public function getDelta($targetToken)
        {
            $ret = false;

            $deltaFile = $this->incremental . '-' . $targetToken->incremental . '.zip';
            $deltaFilePath = Flight::cfg()->get('realBasePath') . '/builds/delta/' . $deltaFile;

            if (file_exists($deltaFilePath)) {
                $ret = array(
                'filename' => $deltaFile,
                'timestamp' => filemtime($deltaFilePath),
                'md5' => $this->getMD5($deltaFilePath),
                'url' => $this->_getUrl($deltaFile, Flight::cfg()->get('deltasPath')),
                'api_level' => $this->apiLevel,
                'incremental' => $targetToken->incremental
              );
            }

            return $ret;
        }

        /**
         * Check version of current build against given build(returns false if lesser version)
         * @param object An object representing current build
         * @return boolean
         */
        public function checkVersion($currentBuild)
        {
            if (empty($currentBuild)) {
                return true;
            } // Valid build if no current build specified

            if ($currentBuild->getTimestamp() >= $this->getTimestamp()) {
                return false; // Invalid build if timestamp older or equal(same build invalid)
            }

            return(version_compare($currentBuild->getVersion(), $this->getVersion(),  "<="));
        }

        /**
         * Return the MD5 value of the current build
         * @param string $path The path of the file
         * @return string The MD5 hash
         */
        public function getMD5($path = '')
        {
            $ret = '';

            if (empty($path)) {
                $path = $this->filePath;
            }

            if (file_exists($path . ".md5sum")) {
                $tmp = explode("  ", file_get_contents($path . '.md5sum'));
                $ret = $tmp[0];
            } elseif ($this->commandExists('md5sum')) {
                $dir = rtrim($path, $this->filename);
                exec("cd $dir && md5sum " . $this->filename. ' > '.$path.'.md5sum');
                $tmp = explode("  ", file_get_contents($path . '.md5sum'));
                $ret = $tmp[0];
            } else {
                $ret = md5_file($path);
            }

            return $ret;
        }


        /* Getters */

        /**
         * Get filesize of the current build
         * @return string filesize in bytes
         */
        public function getSize()
        {
            return $this->size;
        }


        /**
         * Get isUpgradeSupported parameter
         * @return boolean
         */


        /*
        In case android version same(even if migration path not explicitly specified), or migration rules invalid, return $isSameVersion
        Otherwise check if build version is contained in migration paths for given version
        Typical migration_paths.json file would look like:
        {
            "9": [9,10] // i.e. upgrades from 9 to 9 and 9 to 10 are permitted
        }
        */
        public function getIsUpgradeSupported($compareVersion)
        {
            $thisMajorVersion = explode(".", $this->androidVersion)[0];
            $compareMajorVersion = explode(".", $compareVersion)[0];
            $isSameVersion = boolval(!strcmp($thisMajorVersion, $compareMajorVersion)); // Check if Android version is same

            if($isSameVersion) {
                return true;
            }

            else if(file_exists($this->migrationFilePath)) {
                $migrationContents = file_get_contents($this->migrationFilePath);
                if (!empty($migrationContents)) {
                    $migrations = json_decode($migrationContents, true); // Contains migration rules

                    if (!empty($migrations[$compareMajorVersion]) && is_array($migrations[$compareMajorVersion])) {
                        return in_array(intval($thisMajorVersion), $migrations[$compareMajorVersion]);
                    }
                }
            }

            return false;
        }
        /**
         * Get a unique id of the current build
         * @return string A unique id
         */
        public function getUid()
        {
            return $this->uid;
        }

        /**
         * Get the Incremental value of the current build
         * @return string The incremental value
         */
        public function getIncremental()
        {
            return $this->incremental;
        }

        /**
         * Get the API Level of the current build.
         * @return string The API Level value
         */
        public function getApiLevel()
        {
            return $this->apiLevel;
        }

        /**
         * Get the Url of the current build
         * @return string The Url value
         */
        public function getUrl()
        {
            return $this->url;
        }

        /**
         * Get the timestamp of the current build
         * @return string The timestamp value
         */
        public function getTimestamp()
        {
            return $this->timestamp;
        }

        /**
         * Get the changelog Url of the current build
         * @return string The changelog Url value
         */
        public function getChangelogUrl()
        {
            return $this->changelogUrl;
        }

        /**
         * Get the channel of the current build
         * @return string The channel value
         */
        public function getChannel()
        {
            return $this->channel;
        }

        /**
         * Get the filename of the current build
         * @return string The filename value
         */
        public function getFilename()
        {
            return $this->filename;
        }

        /**
         * Get the version of the current build
         * @return string the version value
         */
        public function getVersion()
        {
            return $this->version;
        }

        /**
         * Get the preversion of the current build (alpha, beta.3, etc)
         * @return string the preversion value
         */
        public function getPreversion()
        {
            return $this->preversion;
        }

        /**
         * Get the version of the current build
         * @return string the version value
         */
        public function getDisplayVersion()
        {
            return $this->displayVersion;
        }

        /**
         * Get the Android version of the current build
         * @return string the Android version value
         */
        public function getAndroidVersion()
        {
            return $this->androidVersion;
        }

        /* Utility / Internal */

        /**
         * Return the correct prop file path (depending of version)
         * @return boolean
         */
        private function getPropFilePath()
        {
            return file_exists($this->filePath.'.prop') ?
            $this->filePath.'.prop'
            : 'zip://'.$this->filePath.'#system/build.prop';
        }

        /* Utility / Internal */
        /**
         * Parse version string(semantic versioning) and return array with major and minor version numbers
         * @param string A string representing version to parse
         * @return array
         */
        private function parseSemVer($versionString)
        {
            $versionArray = explode(".", $versionString);
            if(empty($versionArray[0])) {
                $versionArray[0] = 0;
            }
            if(empty($versionArray[1])) {
                $versionArray[1] = 0;
            }
            if(empty($versionArray[2])) {
                $versionArray[2] = 0;
            }
            return array_map("intval", $versionArray);
        }

        /* Utility / Internal */

        /**
         * Remove trailing dashes and flatten match result into array of strings
         * @param array $tokens Array of arrays containing tokens as gotten from filename
         * @return array Array of strings containing tokens
         */
        private function formatTokens($tokens)
        {
            return array_map(function ($token) {
                if (isset($token[0]) && !empty($token[0])) {
                    return trim($token[0], '-');
                } else {
                    return '';
                }
            }, $tokens);
        }

        /**
         * Get the current channel of the build based on the current token
         * @param string $token The channel obtained from build.prop
         * @param string $type The ROM type from filename
         * @param string $version The ROM version from filename
         * @return string The correct channel to be returned
         */
        private function _getChannel($token, $type, $version)
        {
            $ret = 'stable';

            $token = strtolower($token);
            if ($token > '') {
                $ret = $token;
            }

            return $ret;
        }

        /**
         * Get the correct URL for the build
         * @param string $fileName The name of the file
         * @return string The absolute URL for the file to be downloaded
         */
        private function _getUrl($fileName = '', $basePath)
        {
            $prop = $this->getBuildPropValue('ro.build.ota.url');
            if (!empty($prop)) {
                return $prop;
            }

            if (empty($fileName)) {
                $fileName = $this->filename;
            }
            return $basePath . '/' . $fileName;
        }

        /**
         * Get the changelog URL for the current build
         * @return string The changelog URL
         */
        private function _getChangelogUrl()
        {
            if (file_exists(str_replace('.zip', '.txt', $this->filePath))) {
                $ret = str_replace('.zip', '.txt', $this->url);
            } elseif (file_exists(str_replace('.zip', '.html', $this->filePath))) {
                $ret = str_replace('.zip', '.html', $this->url);
            } else {
                $ret = '';
            }

            return $ret;
        }

        /**
         * Get a property value based on the $key value.
         * It does it by searching inside the file build.prop of the current build.
         * @param string $key The key for the wanted value
         * @param string $fallback The fallback value if not found in build.prop
         * @return string The value for the specified key
         */
        private function getBuildPropValue($key, $fallback = null)
        {
            $ret = $fallback ?: null;

            if ($this->buildProp) {
                foreach ($this->buildProp as $line) {
                    if (strpos($line, $key) !== false) {
                        $tmp = explode('=', $line);
                        $ret = $tmp[1];
                        break;
                    }
                }
            }

            return $ret;
        }

        /**
         * Checks if a command is available on the current server
         * @param string $cmd The current command to execute
         * @return boolean Return True if available, False if not
         */
        private function commandExists($cmd)
        {
            if (!$this->functionEnabled('shell_exec')) {
                return false;
            }

            $returnVal = shell_exec("which $cmd");
            return (empty($returnVal) ? false : true);
        }

        /**
         * Checks if a php function is available on the server
         * @param string $func The function to check for
         * @return boolean true if the function is enabled, false if not
         */
        private function functionEnabled($func)
        {
            return is_callable($func) && false === stripos(ini_get('disable_functions'), $func);
        }

        /**
         * Check if the current build is valid with multiple conditions
         * @param type $params The params dictionary inside the current POST request
         * @return boolean True if valid, False if not.
         */
        public function includeInResults($params, $currenteOSVersion)
        {
            return $this->isValid($params) && $this->checkRollout() && $this->checkRequirements($currenteOSVersion) && $this->getSize() > 0;
        }

        /**
         * Return the correct config file path (depending of version)
         * @return string
         */
        private function getConfigFilePath()
        {
            return file_exists($this->filePath . '.config.json') ? $this->filePath . '.config.json' : '';
        }

        /**
         * Return the all values from config file path
         * @return JSON if valid otherwise null
         */
        private function getConfProp(){
            $configFilePath = $this->getConfigFilePath();
            return ($configFilePath) ?  json_decode( file_get_contents($configFilePath) , true) : array();
        }

        /**
         * Return if requirements are met or not for a build
         * @return boolean
         */ 
        public function checkRequirements($currenteOSVersion="")
        {
            $minimalVersion = isset($this->confProp['requirements']) ? $this->confProp['requirements']['minOSVersion'] : null;
            $isCompatible = true; 
            if(isset($minimalVersion)){
               $currenteOSVersionSemantic = $this->parseSemVer($currenteOSVersion);
               $minimalSemanticVersion = $this->parseSemVer($minimalVersion);
               $isCompatible = false;
               if($currenteOSVersionSemantic[0] == $minimalSemanticVersion[0]){
                  if($currenteOSVersionSemantic[1] == $minimalSemanticVersion[1]){ 
                     $isCompatible = $currenteOSVersionSemantic[2] >= $minimalSemanticVersion[2];
                  }
                  else
                     $isCompatible = $currenteOSVersionSemantic[1] > $minimalSemanticVersion[1];
               }
               else
                  $isCompatible = $currenteOSVersionSemantic[0] > $minimalSemanticVersion[0];
            }
            return $isCompatible;
        }

        /**
         * Return if rollout successful or not for a build
         * @return boolean
         */

        public function checkRollout()
        {
            $rolloutpercentage = isset($this->confProp['rollout']['percentage']) ? (int) $this->confProp['rollout']['percentage'] : 100;
            if ($rolloutpercentage < 0 || $rolloutpercentage > 100) {
                return TRUE;
            }
            $rand_number = rand(1, 100);
            if ($rand_number > $rolloutpercentage) {
                return FALSE;
            }
            return TRUE;
        }
    }
