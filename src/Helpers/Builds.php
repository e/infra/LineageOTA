<?php
    /*
        The MIT License (MIT)

        Copyright (c) 2020 Julian Xhokaxhiu

        Permission is hereby granted, free of charge, to any person obtaining a copy of
        this software and associated documentation files (the "Software"), to deal in
        the Software without restriction, including without limitation the rights to
        use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
        the Software, and to permit persons to whom the Software is furnished to do so,
        subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
        FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
        COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
        IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
        CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */

    namespace JX\CmOta\Helpers;

    use \Flight;
    use \JX\CmOta\Helpers\Build;

    class Builds
    {

        // This will contain the build list based on the current request
        private $builds = array();

        private $postData = array();

        private $logger = null;

        private $currentBuild = null;
  
        /**
         * Constructor of the Builds class.
         */
        public function __construct($logger)
        {
            $this->logger = $logger;
            // Set required paths for properly builds Urls later
            Flight::cfg()->set('buildsPath', Flight::cfg()->get('basePath') . '/builds/full');
            Flight::cfg()->set('deltasPath', Flight::cfg()->get('basePath') . '/builds/delta');

            // Get the current POST request data
            $this->postData = Flight::request()->data;

            // Internal Initialization routines
            $this->getBuilds();
        }

        /**
         * Return a valid response list of builds available based on the current request
         * @return array An array preformatted with builds
         */
        public function get()
        {
            $ret = array();

            foreach ($this->builds as $build) {
               
                if (!$build->checkVersion($this->currentBuild)) {
                    $this->logger->debug($build->getIncremental().' ignored as build version is older than current version');
                    continue;
                }
                $isUpgradeSupported = false;
                if (!is_null($this->currentBuild)) {
                    $currentBuildVersion = $this->currentBuild->getAndroidVersion();
                    $isUpgradeSupported = $build->getIsUpgradeSupported($currentBuildVersion);
                    if (!$isUpgradeSupported) {
                        $this->logger->debug($build->getIncremental().' ignored as upgrade path is not permitted');
                        continue;
                    }
                }

                if (preg_match("/disabled/i", $build->getFilename())) {
                    continue;
                }
                
                $this->logger->debug($build->getIncremental().' is a new update');

                array_push($ret, array(
                    // CyanogenMod
                    'incremental' => $build->getIncremental(),
                    'api_level' => $build->getApiLevel(),
                    'url' => $build->getUrl(),
                    'timestamp' => $build->getTimestamp(),
                    'md5sum' => $build->getMD5(),
                    'changes' => $build->getChangelogUrl(),
                    'channel' => $build->getChannel(),
                    'filename' => $build->getFilename(),
                    // LineageOS
                    'romtype' => $build->getChannel(),
                    'datetime' => $build->getTimestamp(),
                    'version' => $build->getVersion(),
                    'pre_version' => $build->getPreversion(),
                    'display_version' => $build->getDisplayVersion(),
                    'android_version' => $build->getAndroidVersion(),
                    'id' => $build->getUid(),
                    'size' => $build->getSize(),
                    'is_upgrade_supported' => $isUpgradeSupported
                ));
            }

            return $ret;
        }
        /**
         * Set a custom set of POST data. Useful to hack the flow in case the data doesn't come within the body of the HTTP request
         * @param array An array structured as POST data
         * @return void
         */
        public function setPostData($customData)
        {
            $this->postData = $customData;
            $this->builds = array();
            $this->getBuilds();
        }

        /**
         * Return a valid response of the delta build (if available) based on the current request
         * @return array An array preformatted with the delta build
         */
        public function getDelta()
        {
            $ret = false;

            $source = $this->postData['source_incremental'];
            $target = $this->postData['target_incremental'];
            if ($source != $target) {
                $sourceToken = null;
                foreach ($this->builds as $build) {
                    if ($build->getIncremental() == $target) {
                        $delta = $sourceToken->getDelta($build);
                        $ret = array(
                            'date_created_unix' => $delta['timestamp'],
                            'filename' => $delta['filename'],
                            'download_url' => $delta['url'],
                            'api_level' => $delta['api_level'],
                            'md5sum' => $delta['md5'],
                            'incremental' => $delta['incremental']
                        );
                    } elseif ($build->getIncremental() == $source) {
                        $sourceToken = $build;
                    }
                }
            }

            return $ret;
        }

        /* Utility / Internal */

        private function getBuilds()
        {
            $params = $this->postData['params'];
            preg_match_all('/.*(eOS) v([0-9\.]+)-?((alpha|beta|rc)(\.\d)?)?.*/', $_SERVER['HTTP_USER_AGENT'], $currentTokens);
            $shouldDisplayPatch=false;
            $this->currenteOSVersion = -1;
            if(isset($currentTokens[2]) && count($currentTokens[2])>0){
                $this->currenteOSVersion = $currentTokens[2][0];
                $versionArray = explode(".", $this->currenteOSVersion);
                if($versionArray[0] > 1 || $versionArray[0] == 1 && $versionArray[1] > 5)
                    $shouldDisplayPatch = true;
            }
            $device = isset($params['device']) ? $params['device'] : '';
            $channels = isset($params['channels']) ? $params['channels'][0] : '';

            // Get physical paths of where the files resides
            $path = Flight::cfg()->get('realBasePath') . '/builds/full';

            $dir = $path . '/' . $channels . '/' . $device;
            if(!is_dir($dir)){
                return;
            }
            // Get the file list and parse it
            $files = scandir($dir);
            if (count($files) > 2) {
                $all_builds = array();
                foreach ($files as $file) {
                    // Skip all files except for the ones that match the pattern
                    // filename starting with e- and ending with .zip are considered as valid files
                    if (!preg_match("/^e-[0-9\.]+-(.*)-$device\.zip$/i", $file) && !($device === "2e" && preg_match("/^e-[0-9\.]+-(.*)-zirconia\.zip$/i", $file))) {
                        continue;
                    }
                    $build = null;

                    // If APC is enabled
                    if (extension_loaded('apcu') && ini_get('apc.enabled')) {
                        $build = apcu_fetch($file);

                        // If not found there, we have to find it with the old fashion method...
                        if ($build === false) {
                            $build = new Build($file, $dir, $this->logger, $shouldDisplayPatch);
                            // ...and then save it for 72h until it expires again
                            apcu_store($file, $build, 72*60*60);
                        }
                    } else {
                        $build = new Build($file, $dir, $this->logger, $shouldDisplayPatch);
                    }
                    array_push($all_builds, $build);

                    $sourceIncremental = isset($this->postData['params']['source_incremental']) ? $this->postData['params']['source_incremental'] : NULL;
                    if ($build->isValid($this->postData['params']) && $sourceIncremental && strcmp($sourceIncremental, $build->getIncremental()) === 0) {
                        $this->currentBuild = $build;
                        $this->logger->debug($build->getIncremental().' is the current build');
                        if($this->currenteOSVersion === -1){
                            //get current version from build.prop
                            $this->currenteOSVersion = $this->currentBuild->getVersion();
                        }
                    }
                }

                $this->logger->debug('Current version: '.$this->currenteOSVersion);

                foreach ($all_builds as $build){
                    if ($build->includeInResults($this->postData['params'], $this->currenteOSVersion)) {
                        array_push($this->builds, $build);
                    }
                }

            }
            $this->logger->debug('Total execution  time  of getBuilds in seconds');
        }

}
